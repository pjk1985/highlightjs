<?php
/**
 * @brief Highlight.js
 **/
class highlightjs extends EditorHandler
{
	var $editor_sequence = 0;
	var $component_path = '';
	var $brushes = array(
		array('Automatic', 		'', ''),
		array('Apache', 		'apache', ''),
		array('Bash', 			'bash', ''),
		array('C#', 			'cs', ''),
		array('C++', 			'cpp', ''),
		array('CSS', 			'css', ''),
		array('CoffeeScript', 	'coffeescript', ''),
		array('Diff', 			'diff', ''),
		array('HTML, XML', 		'html', ''),
		array('Ini, TOML', 		'ini', ''),
		array('JSON', 			'json', ''),
		array('Java', 			'java', ''),
		array('JavaScript', 	'javascript', ''),
		array('Markdown', 		'markdown', ''),
		array('Nginx', 			'nginx', ''),
		array('Objective-C', 	'objectivec', ''),
		array('PHP', 			'php', ''),
		array('Perl', 			'perl', ''),
		array('Python', 		'python', ''),
		array('Ruby', 			'ruby', ''),
		array('SQL', 			'sql', ''),
		array('Shell Session', 	'shell', ''),
		array('ARM Assembly', 	'armasm', ''),
		array('AVR Assembler', 	'avrasm', ''),
		array('CMake', 			'cmake', ''),
		array('Delphi', 		'delphi', ''),
		array('Fortran', 		'fortran', ''),
		array('G-code (ISO 6983)', 'gcode', ''),
		array('GLSL', 			'glsl', ''),
		array('Go', 			'go', ''),
		array('Groovy', 		'groovy', ''),
		array('Lisp', 			'lisp', ''),
		array('Lua', 			'lua', ''),
		array('Mathematica', 	'mathematica', ''),
		array('Matlab', 		'matlab', ''),
		array('PowerShell', 	'powershell', ''),
		array('QML', 			'qml', ''),
		array('Swift', 			'swift', ''),
	);

	function highlightjs($editor_sequence, $component_path)
	{
		$this->editor_sequence = $editor_sequence;
		$this->component_path = $component_path;
	}

	function getPopupContent()
	{
		$tpl_path = $this->component_path.'tpl';
		$tpl_file = 'popup.html';
		$script_path = $this->component_path.'highlight/';
		
		if(!$this->theme) $this->theme = 'default';
		$theme_file = $this->component_path.'highlight/styles/'.$this->theme.'.css';

		Context::set('tpl_path', $tpl_path);
		Context::set('script_path', $script_path);
		Context::set('brushes', $this->brushes);
		Context::addCSSFile($theme_file);
		Context::addJsFile($script_path.'highlight.pack.js');

		$oTemplate = &TemplateHandler::getInstance();
		return $oTemplate->compile($tpl_path, $tpl_file);
	}

	function transHTML($xml_obj)
	{
		$script_path = getScriptPath().'modules/editor/components/highlightjs/highlight/';
		$code_type = $xml_obj->attrs->code_type;
		$option_title = ' title="'.$xml_obj->attrs->title.'"';
		$body = $xml_obj->body;
		$body = strip_tags($body, '<br>');
		$body = preg_replace("/(<br\s*\/?>)(\n|\r)*/i", "\n", $body);
		$body = strip_tags($body);
		$body = str_replace('&nbsp;', ' ', $body);
	
		if(!$GLOBALS['_called_editor_component_highlightjs_'])
		{
			$GLOBALS['_called_editor_component_highlightjs_'] = true;

			$sh_js_code[] = '<script type="text/javascript">';
			$sh_js_code[] = 'hljs.initHighlightingOnLoad();';
			$sh_js_code[] = '</script>';

			if(!$this->theme) $this->theme = 'default';
			$theme_file = $this->component_path.'highlight/styles/'.$this->theme.'.css';

			Context::set('script_path', $script_path);
			Context::addHtmlFooter(implode(PHP_EOL, $sh_js_code));
			Context::addCSSFile($theme_file);
			Context::addJsFile($script_path.'highlight.pack.js');
		}

		$output = sprintf('<pre><code class="%s">%s</code></pre>',
			$code_type,
			$body
		);

		return $output;
	}
}
