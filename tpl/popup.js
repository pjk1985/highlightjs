﻿function debugPrint(msg) { if(typeof console == 'object' && typeof console.log == 'function') { console.log(msg); } }

function getCode()
{
	"use strict";
	debugPrint('>>> getCode()');
	if(typeof(opener) == "undefined") return;

	var node = opener.editorPrevNode;
	var form$ = jQuery('#fo');

	if(!node || node.nodeName != 'DIV')
	{
		var code = opener.editorGetSelectedHtml(opener.editorPrevSrl);
		code = getArrangedCode(code, 'textarea');
		form$.find('textarea[name=code]').val(code);
		return;
	}

	var opt = getArrangedOption(jQuery(node));
	opt.code = getArrangedCode(opt.code, 'textarea');

	form$.find('select[name=code_type]').val(opt.code_type);
	form$.find('input[name=title]').val(opt.title);
	form$.find('textarea[name=code]').val(opt.code);
}

function insertCode()
{
	"use strict";
	debugPrint('>>> insertCode()');
	if(typeof(opener) == "undefined") return;

	var form$ = jQuery('#fo');
	var opt = getArrangedOption(form$);
	opt.code = getArrangedCode(opt.code, 'wyswig');

	var style = "font-family:'DejaVu Sans Mono', 'Courier New', Courier, monospace !important; border:#666 1px dotted;border-left:#2AE 5px solid;padding:5px;background:#FAFAFA url('./modules/editor/components/highlightjs/component_icon.gif') no-repeat top right;";
	var html = '<div editor_component="highlightjs" code_type="'+opt.code_type+'" style="'+style+'">'+opt.code+'</div><br />';

	var iframe_obj = opener.editorGetIFrame(opener.editorPrevSrl);
	var prevNode = opener.editorPrevNode;

	if (prevNode && prevNode.nodeName == 'DIV' && prevNode.getAttribute('editor_component') != null) {
		prevNode.setAttribute('code_type', opt.code_type);
		prevNode.setAttribute('title', opt.title);
		prevNode.setAttribute('style', style);
		prevNode.innerHTML = opt.code;
		debugPrint('innerHTML');
	}
	else
	{
		opener.editorReplaceHTML(iframe_obj, html);
		debugPrint('editorReplaceHTML');
	}
	opener.editorFocus(opener.editorPrevSrl);

	window.close();
}

function getArrangedOption(elem$)
{
	"use strict";
	debugPrint('>>> getArrangedOption()');
	if(!elem$.size()) return;

	var node = elem$[0];
	var opt = {};

	if(node.nodeName == 'FORM')
	{
		opt.code_type = elem$.find('select[name=code_type]').val();
		opt.title = elem$.find('input[name=title]').val();
		opt.code = elem$.find('textarea[name=code]').val();
	}
	else
	{
		opt.code_type = node.getAttribute('code_type');
		opt.title = node.getAttribute('title');
		opt.code = elem$.html();
	}
	
	return opt;
}

function getArrangedCode(code, outputType)
{
	"use strict";
	debugPrint('>>> getArrangedCode()');
	if(!outputType) outputType = 'textarea';

	if(outputType == 'wyswig')
	{
		code = code.replace(/</g, "&lt;");
		code = code.replace(/>/g, "&gt;");
		code = code.replace(/ /g, "&nbsp;");
		code = code.replace(/\n/g, "<br />\n");
	}

	if(outputType == 'textarea')
	{
		code = code.replace(/\r|\n/g, '');
		code = code.replace(/<\/p>/gi, "\n");
		code = code.replace(/<br\s*\/?>/gi, "\n");
		code = code.replace(/(<([^>]+)>)/gi,"");;
		code = code.replace(/&nbsp;/g, ' ');
		code = code.replace(/&lt;/g, '<');
		code = code.replace(/&gt;/g, '>');
	}
	else if(outputType == 'preview')
	{
		code = code.replace(/</g, '&lt;');
		code = code.replace(/>/g, '&gt;');
	}

	code = jQuery.trim(code);

	return code;
}

